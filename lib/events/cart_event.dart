import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class CartEvent extends Equatable {
  const CartEvent();
}

class AddFoodToCart extends CartEvent {
  final String id;
  final String name;
  final String price;
  final String description;
  final String urlImage;
  final BuildContext context;
  final num quantity;
  final int index;
  const AddFoodToCart(
      {required this.name,
      required this.id,
      required this.price,
      required this.description,
      required this.index,
      required this.urlImage,
      required this.context,
      required this.quantity});

  @override
  // TODO: implement props
  List<Object?> get props =>
      [name, price, description, urlImage, context, quantity];
}
