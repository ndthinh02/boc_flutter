import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

@immutable
abstract class FoodEvent extends Equatable {
  const FoodEvent();
}

class FetchFood extends FoodEvent {
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class AddFood extends FoodEvent {
  final String name;
  final String price;
  final String description;
  final String urlImage;
  final BuildContext context;
  const AddFood(
      {required this.name,
      required this.price,
      required this.description,
      required this.urlImage,
      required this.context});

  @override
  // TODO: implement props
  List<Object?> get props => [name, price, description, urlImage];
}
