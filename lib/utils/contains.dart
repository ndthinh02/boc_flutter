import 'package:flutter/cupertino.dart';

class Contains {
  final TextEditingController nameController = TextEditingController();
  final TextEditingController priceController = TextEditingController();
  final TextEditingController desccriptionController = TextEditingController();
  final TextEditingController urlImageController = TextEditingController();
  clear() {
    nameController.clear();
    priceController.clear();
    desccriptionController.clear();
    urlImageController.clear();
  }
}
