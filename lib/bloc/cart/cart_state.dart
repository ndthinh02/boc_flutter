import 'package:bloc_test/models/cart.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

@immutable
abstract class CartState extends Equatable {
  const CartState();
}

class CartInitial extends CartState {
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class AddFoodToCartSucces extends CartState {
  List<Cart> mListCart = [];
  AddFoodToCartSucces({required this.mListCart});
  @override
  // TODO: implement props
  List<Object?> get props => [mListCart];
}
