import 'package:bloc/bloc.dart';
import 'package:bloc_test/bloc/cart/cart_state.dart';
import 'package:bloc_test/events/cart_event.dart';
import 'package:bloc_test/models/cart.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  final List<Cart> _mListCart = [];
  CartBloc() : super(CartInitial()) {
    on<AddFoodToCart>((event, emit) {
      try {
        bool check = false;
        for (var element in _mListCart) {
          if (element.id == event.id) {
            _mListCart[event.index].quantity =
                _mListCart[event.index].quantity + 1;
            check = true;
          }
        }
        if (check) {
        } else {
          Cart cart = Cart(
              id: event.id,
              name: event.name,
              urlImage: event.urlImage,
              price: event.price,
              description: event.description,
              quantity: event.quantity,
              index: event.index);
          _mListCart.add(cart);
        }
      } catch (e) {}

      emit(AddFoodToCartSucces(mListCart: _mListCart));
    });
  }
}
