import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '../../models/foods.dart';

@immutable
abstract class FoodState extends Equatable {
  const FoodState();
}

class FoodInitial extends FoodState {
  @override
  List<Object> get props => [];
}

class FoodLoadingState extends FoodState {
  @override
  List<Object?> get props => [];
}

class FoodLoadedState extends FoodState {
  final List<Foods>? foods;

  const FoodLoadedState({required this.foods});
  @override
  List<Object?> get props => [foods];
}

class AddFoodLoading extends FoodState {
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class AddFoodSuccess extends FoodState {
  final Foods foods;
  const AddFoodSuccess({required this.foods});
  @override
  // TODO: implement props
  List<Object?> get props => [foods];
}
