import 'package:bloc_test/events/foods_event.dart';
import 'package:bloc_test/repo/foods_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../views/foods/add_foods_screen.dart';
import 'food_state.dart';

class FoodBloc extends Bloc<FoodEvent, FoodState> {
  final FoodRepository _foodRepository;
  clear() {
    nameController.clear();
    priceController.clear();
    urlImageController.clear();
    desccriptionController.clear();
  }

  FoodBloc(this._foodRepository) : super(FoodInitial()) {
    on<FetchFood>((event, emit) async {
      emit(FoodLoadingState());

      try {
        final foods = await _foodRepository.fetchAllFoods();
        emit(FoodLoadedState(foods: foods));
      } catch (e) {
        e;
      }
    });
    on<AddFood>((event, emit) async {
      emit(FoodLoadingState());
      try {
        showDialog(
          context: event.context,
          builder: (context) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          },
        );
        final mListfoods = await _foodRepository.fetchAllFoods();
        final foods = await _foodRepository
            .addFoodToSever(
                event.name, event.price, event.description, event.urlImage)
            .whenComplete(() => Navigator.of(event.context).pop())
            .whenComplete(
                () => Fluttertoast.showToast(msg: "Add succesfully !"))
            .whenComplete(() => clear());
        emit(AddFoodSuccess(foods: foods!));
        emit(FoodLoadedState(foods: mListfoods));
      } catch (e) {
        rethrow;
      }
    });
  }
}
