import 'dart:convert';

import 'package:http/http.dart' as http;

import '../models/foods.dart';

class FoodRepository {
  var url = Uri.parse("https://63eafe34f1a969340db02811.mockapi.io/food");
  Future<List<Foods>?> fetchAllFoods() async {
    final response = await http.get(url);
    if (response.statusCode == 200) {
      // print(response.body);
      List<dynamic> body = jsonDecode(utf8.decode(response.bodyBytes));
      List<Foods> foods = body.map((e) => Foods.fromJson(e)).toList();
      return foods;
    } else {
      throw Exception("Failed to fetch");
    }
  }

  Future<Foods?> addFoodToSever(
      String name, price, description, urlImage) async {
    final body = jsonEncode({
      "name": name,
      "price": price,
      "description": description,
      "urlImage": urlImage,
    });

    final response = await http
        .post(url, body: body, headers: {"Content-Type": "application/json"});
    Foods food = Foods.fromJson(jsonDecode(response.body));
    return food;
  }
}
