import 'package:flutter/animation.dart';

const colorGrey = Color(0xFFEDEDED);
const colorButton = Color(0xFFD9D9D9);
const colorSave = Color(0xFF00A328);
