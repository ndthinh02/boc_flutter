import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  String subTitle;
  GestureDetector? gestureDetector;
  bool showPass;
  TextEditingController controller;
  bool? isReadOnly;
  String titles;
  String requiredMent;
  TextInputType? textInputType;
  CustomTextField(
      {super.key,
      this.textInputType,
      required this.requiredMent,
      required this.titles,
      required this.isReadOnly,
      required this.controller,
      required this.subTitle,
      this.gestureDetector,
      required this.showPass});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        RichText(
          text: TextSpan(
            // Note: Styles for TextSpans must be explicitly defined.
            // Child text spans will inherit styles from parent
            style: const TextStyle(
              color: Colors.black,
            ),
            children: <TextSpan>[
              TextSpan(
                  text: titles,
                  style: const TextStyle(
                      color: Colors.black, fontWeight: FontWeight.w600)),
              TextSpan(
                  text: requiredMent,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.red)),
            ],
          ),
        ),
        const SizedBox(
          height: 14,
        ),
        TextField(
          keyboardType: textInputType,
          readOnly: isReadOnly!,
          controller: controller,
          obscureText: showPass,
          decoration: InputDecoration(
              suffixIcon: Padding(
                padding: const EdgeInsets.all(8.0),
                child: gestureDetector,
              ),
              border: const OutlineInputBorder(),
              hintText: subTitle),
        )
      ],
    );
  }
}
