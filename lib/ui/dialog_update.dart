import 'package:bloc_test/ui/text_form_filed.dart';
import 'package:bloc_test/views/todolist/home_screen.dart';
import 'package:flutter/material.dart';

import '../models/user.dart';
import 'color.dart';

class DialogUpdate extends StatefulWidget {
  final User user;
  final Function updateUser;
  final int index;

  const DialogUpdate({
    super.key,
    required this.user,
    required this.updateUser,
    required this.index,
  });

  @override
  State<DialogUpdate> createState() => _DialogUpdateState();
}

final TextEditingController nameControllerUpdate = TextEditingController();
final TextEditingController sexControllerUpdate = TextEditingController();
final TextEditingController idControllerUpdate = TextEditingController();
String maleUpdate = '';

class _DialogUpdateState extends State<DialogUpdate> {
  final _itemSexUpdate = ['Male', 'Female'];

  bool isShowPass = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    nameControllerUpdate.text = mlistUser[widget.index].name;
    idControllerUpdate.text = mlistUser[widget.index].id;
    maleUpdate = mlistUser[widget.index].sex;
  }

  @override
  Widget build(BuildContext context) {
    return StatefulBuilder(
      builder: (context, setState) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
          child: Padding(
            padding: const EdgeInsets.all(24.0),
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  const Text(
                    "EDIT",
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  CustomTextField(
                    controller: nameControllerUpdate,
                    showPass: false,
                    subTitle: widget.user.name,
                    isReadOnly: false,
                    requiredMent: '*',
                    titles: 'Full name',
                  ),
                  const SizedBox(
                    height: 14,
                  ),
                  CustomTextField(
                    isReadOnly: true,
                    gestureDetector: GestureDetector(
                      onTap: () {},
                      child: DropdownButton(
                        onChanged: (newValue) {
                          setState(() {
                            maleUpdate = newValue!;
                          });
                        },
                        underline: const SizedBox(
                          height: 0,
                        ),
                        icon: const Icon(Icons.keyboard_arrow_down),
                        items: _itemSexUpdate
                            .map((e) => DropdownMenuItem(
                                  value: e.toString(),
                                  child: Text(e),
                                ))
                            .toList(),
                      ),
                    ),
                    controller: sexControllerUpdate,
                    showPass: false,
                    subTitle: maleUpdate,
                    requiredMent: '',
                    titles: 'Sex',
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  CustomTextField(
                    controller: idControllerUpdate,
                    gestureDetector: GestureDetector(
                      onTap: () {
                        setState(() {
                          isShowPass = !isShowPass;
                        });
                      },
                      child: Icon(isShowPass
                          ? Icons.visibility_off_outlined
                          : Icons.visibility_outlined),
                    ),
                    showPass: isShowPass,
                    subTitle: widget.user.id,
                    isReadOnly: false,
                    requiredMent: '',
                    titles: 'Citizen Id',
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        style: ElevatedButton.styleFrom(
                            backgroundColor: colorButton,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8))),
                        child: const Text(
                          "Cancel",
                          style: TextStyle(color: Colors.black),
                        ),
                      ),
                      ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: colorSave,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8))),
                          onPressed: () {
                            setState(() {
                              widget.updateUser();
                              Navigator.of(context).pop();
                            });
                          },
                          child: const Text("Save")),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
