import 'package:flutter/material.dart';

class CustomDialog extends StatelessWidget {
  final String title;
  final String description;
  VoidCallback edit;
  VoidCallback delete;

  CustomDialog({
    required this.edit,
    required this.delete,
    super.key,
    required this.title,
    required this.description,
  });

  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 0,
      backgroundColor: const Color(0xffffffff),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      child: Column(mainAxisSize: MainAxisSize.min, children: [
        const SizedBox(height: 15),
        Text(
          title,
          style: const TextStyle(
            fontSize: 18.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(height: 15),
        const Divider(
          height: 1,
        ),
        SizedBox(
          width: MediaQuery.of(context).size.width,
          height: 50,
          child: InkWell(
            highlightColor: Colors.grey[200],
            onTap: () {
              edit();
            },
            child: Center(
              child: Text(
                "Edit",
                style: TextStyle(
                  fontSize: 18.0,
                  color: Theme.of(context).primaryColor,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ),
        const Divider(
          height: 1,
        ),
        SizedBox(
          width: MediaQuery.of(context).size.width,
          height: 50,
          child: InkWell(
            borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(15.0),
              bottomRight: Radius.circular(15.0),
            ),
            highlightColor: Colors.grey[200],
            onTap: () {
              delete();
            },
            child: const Center(
              child: Text(
                "Delete",
                style: TextStyle(
                  color: Colors.red,
                  fontSize: 16.0,
                  fontWeight: FontWeight.normal,
                ),
              ),
            ),
          ),
        ),
        const Divider(
          height: 1,
        ),
        SizedBox(
          width: MediaQuery.of(context).size.width,
          height: 50,
          child: InkWell(
            borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(15.0),
              bottomRight: Radius.circular(15.0),
            ),
            highlightColor: Colors.grey[200],
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Center(
              child: Text(
                "Cancel",
                style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontSize: 16.0,
                  fontWeight: FontWeight.normal,
                ),
              ),
            ),
          ),
        ),
      ]),
    );
  }
}
