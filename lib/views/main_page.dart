import 'package:bloc_test/views/foods/bottom/home_page_bottom.dart';
import 'package:bloc_test/views/todolist/home_screen.dart';
import 'package:flutter/material.dart';

class MainPage extends StatelessWidget {
  const MainPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => const MyHomePage(),
                  ));
                },
                child: const Text("Todo list")),
            ElevatedButton(
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => const HomePageBottom(),
                  ));
                },
                child: const Text("Foods "))
          ],
        ),
      ),
    );
  }
}
