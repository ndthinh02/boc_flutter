import 'package:bloc_test/ui/dialog_update.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../models/user.dart';

class Item extends StatefulWidget {
  final User user;
  final int index;
  final Function deleteUser, updateUser;
  const Item({
    super.key,
    required this.user,
    required this.updateUser,
    required this.deleteUser,
    required this.index,
  });

  @override
  State<Item> createState() => _ItemState();
}

class _ItemState extends State<Item> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        showCupertinoDialog(
          context: context,
          builder: (context) {
            return Center(
              child: CupertinoActionSheet(
                title: const Text('Choose options'),
                cancelButton: CupertinoActionSheetAction(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('Cancel'),
                ),
                actions: <CupertinoActionSheetAction>[
                  CupertinoActionSheetAction(
                    isDefaultAction: true,
                    onPressed: () {
                      Navigator.pop(context);
                      showDialog<void>(
                        barrierDismissible: true,
                        context: context,
                        builder: (BuildContext context) {
                          return DialogUpdate(
                              user: widget.user,
                              updateUser: () {
                                widget.updateUser();
                              },
                              index: widget.index);
                        },
                      );
                    },
                    child: const Text(
                      'Edit',
                      style: TextStyle(color: Colors.red),
                    ),
                  ),
                  CupertinoActionSheetAction(
                    isDefaultAction: true,
                    onPressed: () {
                      Navigator.pop(context);
                      widget.deleteUser();
                    },
                    child: const Text('Delete'),
                  ),
                ],
              ),
            );
          },
        );
      },
      child: Column(
        children: [
          const SizedBox(
            height: 14,
          ),
          ListTile(
              title: Text(
                widget.user.name,
                style: const TextStyle(
                    color: Colors.black, fontWeight: FontWeight.bold),
              ),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.user.sex,
                    style: const TextStyle(
                        color: Colors.black, fontWeight: FontWeight.w400),
                  ),
                  Text(
                    widget.user.id,
                    style: const TextStyle(
                        color: Colors.black, fontWeight: FontWeight.w400),
                  )
                ],
              )),
          const Divider(
            height: 1,
            color: Colors.grey,
          ),
        ],
      ),
    );
  }
}
