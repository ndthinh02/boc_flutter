import 'package:bloc_test/ui/color.dart';
import 'package:bloc_test/ui/dialog_update.dart';
import 'package:bloc_test/ui/text_form_filed.dart';
import 'package:bloc_test/views/todolist/item.dart';
import 'package:flutter/material.dart';

import '../../models/user.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

final List<User> mlistUser = [];

class _MyHomePageState extends State<MyHomePage> {
  final _itemSex = ['Male', 'Female'];
  String male = 'Male';

  final TextEditingController nameController = TextEditingController();
  final TextEditingController sexController = TextEditingController();
  final TextEditingController idController = TextEditingController();

  bool isShowPass = true;

  addUser(String name, String sex, String id) {
    User user = User(name: name, sex: sex, id: id);
    setState(() {
      mlistUser.add(user);
    });
    setState(() {
      nameController.text = "";
      idController.text = "";
    });
  }

  deleteUser(int index) {
    setState(() {
      mlistUser.removeAt(index);
    });
  }

  updateUser(int index) {
    setState(() {
      mlistUser[index].name = nameControllerUpdate.text;
      mlistUser[index].sex = maleUpdate;
      mlistUser[index].id = idControllerUpdate.text;
    });
    clear();
  }

  clear() {
    setState(() {
      nameControllerUpdate.text = "";
      idControllerUpdate.text = "";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: [
          Container(
            height: 20,
            margin: const EdgeInsets.all(14),
            child: ElevatedButton(
              onPressed: () {
                if (nameController.text.isEmpty || idController.text.isEmpty) {
                  const snackBar = SnackBar(
                    content: Text('Is not empty! '),
                    backgroundColor: Colors.green,
                    elevation: 10,
                    behavior: SnackBarBehavior.floating,
                    margin: EdgeInsets.all(5),
                  );
                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                } else {
                  addUser(nameController.text, male, idController.text);
                }
              },
              style: ElevatedButton.styleFrom(
                  backgroundColor: colorGrey,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8))),
              child: const Text(
                'Add',
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
              ),
            ),
          )
        ],
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
              margin: const EdgeInsets.all(20),
              child: Column(
                children: [_buildContent()],
              )),
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Widget _buildContent() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
      child: Column(
        children: [
          CustomTextField(
            controller: nameController,
            isReadOnly: false,
            requiredMent: '*',
            showPass: false,
            subTitle: 'Enter your name',
            titles: 'Full name',
          ),
          const SizedBox(
            height: 5,
          ),
          CustomTextField(
            isReadOnly: true,
            gestureDetector: GestureDetector(
              onTap: () {},
              child: DropdownButton(
                onChanged: (newValue) {
                  setState(() {
                    male = newValue!;
                  });
                },
                underline: const SizedBox(
                  height: 0,
                ),
                icon: const Icon(Icons.keyboard_arrow_down_outlined),
                items: _itemSex
                    .map((e) => DropdownMenuItem(
                          value: e.toString(),
                          child: Text(e),
                        ))
                    .toList(),
              ),
            ),
            controller: sexController,
            showPass: false,
            subTitle: male,
            requiredMent: '',
            titles: 'Sex',
          ),
          const SizedBox(
            height: 10,
          ),
          CustomTextField(
            controller: idController,
            gestureDetector: GestureDetector(
              onTap: () {
                setState(() {
                  isShowPass = !isShowPass;
                });
              },
              child: Icon(isShowPass
                  ? Icons.visibility_off_outlined
                  : Icons.visibility_outlined),
            ),
            showPass: isShowPass,
            subTitle: "Citizen ID",
            isReadOnly: false,
            requiredMent: '',
            titles: 'Citizen ID',
          ),
          const SizedBox(
            height: 40,
          ),
          const Divider(
            height: 5,
            color: Colors.grey,
          ),
          ListView.builder(
            shrinkWrap: true,
            physics: const BouncingScrollPhysics(),
            itemCount: mlistUser.length,
            itemBuilder: (context, index) {
              return Item(
                user: mlistUser[index],
                index: index,
                deleteUser: () {
                  deleteUser(index);
                  print('ok');
                },
                updateUser: () {
                  updateUser(index);
                },
              );
            },
          )
        ],
      ),
    );
  }
}
