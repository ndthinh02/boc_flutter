import 'package:flutter/material.dart';

import '../../models/cart.dart';

class DetailScreen extends StatefulWidget {
  final Cart cart;
  const DetailScreen({super.key, required this.cart});

  @override
  State<DetailScreen> createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.cart.name),
      ),
      body: Column(
        children: [
          Container(
            margin:
                const EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
            width: MediaQuery.of(context).size.width,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Image.network(
                widget.cart.urlImage,
                fit: BoxFit.cover,
              ),
            ),
          ),
          Text(widget.cart.description),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              const Text(
                "Price ",
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
              ),
              Text("\$ ${widget.cart.price}"),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              const Text(
                "Quantity",
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
              ),
              Text("${widget.cart.quantity}"),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: const [
              Text(
                "Total ",
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
              ),
              // Text(" \$ ${cartController.totalAmount}"),
            ],
          ),
        ],
      ),
    );
  }
}
