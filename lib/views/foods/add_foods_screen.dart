import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../bloc/foods/food_bloc.dart';
import '../../events/foods_event.dart';
import '../../ui/text_form_filed.dart';

class AddFoodsScreen extends StatefulWidget {
  const AddFoodsScreen({super.key});

  @override
  State<AddFoodsScreen> createState() => _AddFoodsScreenState();
}

final TextEditingController nameController = TextEditingController();
final TextEditingController priceController = TextEditingController();
final TextEditingController desccriptionController = TextEditingController();
final TextEditingController urlImageController = TextEditingController();

class _AddFoodsScreenState extends State<AddFoodsScreen> {
  void addFood() {
    String name = nameController.text.toString().trim();
    String price = priceController.text.toString().trim();
    String description = desccriptionController.text.toString().trim();
    String urlImage = urlImageController.text.toString().trim();
    if (name.isEmpty ||
        price.isEmpty ||
        description.isEmpty ||
        urlImage.isEmpty) {
      Fluttertoast.showToast(msg: "Is not empty");
    } else {
      BlocProvider.of<FoodBloc>(context).add(AddFood(
          name: name,
          price: price,
          description: description,
          urlImage: urlImage,
          context: context));
      nameController.clear();
      priceController.clear();

      desccriptionController;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: const Icon(
            Icons.arrow_back_ios_outlined,
            size: 14,
          ),
        ),
        centerTitle: true,
        title: const Text("Add"),
      ),
      body: Column(
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Column(
                        children: [
                          CustomTextField(
                              requiredMent: '',
                              titles: 'Name',
                              isReadOnly: false,
                              controller: nameController,
                              subTitle: 'Enter your name',
                              showPass: false),
                          const SizedBox(
                            height: 10,
                          ),
                          CustomTextField(
                              textInputType: TextInputType.number,
                              requiredMent: '',
                              titles: 'Price',
                              isReadOnly: false,
                              controller: priceController,
                              subTitle: 'Enter your price',
                              showPass: false),
                          const SizedBox(
                            height: 10,
                          ),
                          CustomTextField(
                              requiredMent: '',
                              titles: 'Description',
                              isReadOnly: false,
                              controller: desccriptionController,
                              subTitle: 'Enter your description',
                              showPass: false),
                          const SizedBox(
                            height: 10,
                          ),
                          CustomTextField(
                              requiredMent: '',
                              titles: 'Url Image',
                              isReadOnly: false,
                              controller: urlImageController,
                              subTitle: 'Enter your url Image ',
                              showPass: false),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 40,
              child: ElevatedButton(
                onPressed: () {
                  addFood();
                },
                style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.blue,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8))),
                child: const Text("Save"),
              ),
            ),
          )
        ],
      ),
    );
  }
}
