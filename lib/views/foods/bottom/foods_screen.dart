import 'package:bloc_test/events/foods_event.dart';
import 'package:bloc_test/views/foods/add_foods_screen.dart';
import 'package:bloc_test/views/foods/item_foods.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../bloc/foods/food_bloc.dart';
import '../../../bloc/foods/food_state.dart';
import '../../../models/foods.dart';

class FoodScreen extends StatefulWidget {
  const FoodScreen({super.key});

  @override
  State<FoodScreen> createState() => _FoodScreenState();
}

class _FoodScreenState extends State<FoodScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    BlocProvider.of<FoodBloc>(context).add(FetchFood());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Home"),
      ),
      body: BlocBuilder<FoodBloc, FoodState>(builder: (context, state) {
        if (state is FoodLoadingState) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        if (state is FoodLoadedState) {
          List<Foods> foods = state.foods!;
          return ListView.builder(
            itemCount: foods.length,
            itemBuilder: (context, index) {
              return ItemFoods(foods: foods[index], index: index);
            },
          );
        }
        return Container();
      }),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => const AddFoodsScreen(),
          ));
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
