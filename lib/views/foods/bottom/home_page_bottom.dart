import 'package:bloc_test/views/foods/bottom/cart_screen.dart';
import 'package:bloc_test/views/foods/bottom/foods_screen.dart';
import 'package:flutter/material.dart';

class HomePageBottom extends StatefulWidget {
  const HomePageBottom({super.key});

  @override
  State<HomePageBottom> createState() => _HomePageBottomState();
}

class _HomePageBottomState extends State<HomePageBottom> {
  int _selectedIndex = 0;

  final List _page = [const FoodScreen(), const CartScreen()];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: _selectedIndex,
          onTap: _onItemTapped,
          items: [
            BottomNavigationBarItem(
                label: "Home", icon: Image.asset("images/home.png")),
            BottomNavigationBarItem(
                label: "Order", icon: Image.asset("images/vector.png")),
          ]),
      body: _page.elementAt(_selectedIndex),
    );
  }
}
