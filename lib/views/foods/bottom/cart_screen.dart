import 'package:bloc_test/bloc/cart/cart_bloc.dart';
import 'package:bloc_test/bloc/cart/cart_state.dart';
import 'package:bloc_test/models/cart.dart';
import 'package:bloc_test/views/foods/item_order.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({super.key});

  @override
  State<CartScreen> createState() => _OrderScreenState();
}

class _OrderScreenState extends State<CartScreen> {
  // CartController get cartController => context.read<CartController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text("Order"),
        ),
        body: BlocBuilder<CartBloc, CartState>(builder: (context, state) {
          if (state is AddFoodToCartSucces) {
            List<Cart> cart = state.mListCart;

            return Column(
              children: [
                Expanded(
                    child: ListView.builder(
                  itemCount: state.mListCart.length,
                  itemBuilder: (context, index) {
                    return ItemOrder(cart: cart[index]);
                  },
                )),
                Container(
                  padding: const EdgeInsets.all(8),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: const [
                          Text(
                            "Total",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                          // Text("\$ ${cartController.totalAmount.toString()}"),
                        ],
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: const [
                          Text(
                            "Quantity",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                          // Text(cartController.totalQuantity.toString()),
                        ],
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      // Text(cartController.totalQuantity.toString())
                    ],
                  ),
                )
              ],
            );
          }
          return Container();
        }));
  }
}
