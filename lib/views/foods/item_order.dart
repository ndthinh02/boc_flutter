import 'package:bloc_test/views/foods/detais.dart';
import 'package:flutter/material.dart';

import '../../models/cart.dart';

class ItemOrder extends StatefulWidget {
  final Cart cart;
  const ItemOrder({super.key, required this.cart});

  @override
  State<ItemOrder> createState() => _ItemFoodsState();
}

class _ItemFoodsState extends State<ItemOrder> {
  // CartController get readCartController => context.read<CartController>();
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(2),
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => DetailScreen(
              cart: widget.cart,
            ),
          ));
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    margin: const EdgeInsets.only(left: 24, top: 14),
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(8)),
                        image: DecorationImage(
                            fit: BoxFit.cover,
                            image: NetworkImage(widget.cart.urlImage))),
                  ),
                  const SizedBox(
                    width: 24,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const SizedBox(
                        height: 14,
                      ),
                      SizedBox(
                        width: 100,
                        child: Text(
                          widget.cart.id,
                          overflow: TextOverflow.ellipsis,
                          style: const TextStyle(
                              fontSize: 18,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      const SizedBox(
                        height: 12,
                      ),
                      Text(
                        "\$ ${widget.cart.price}",
                        style: const TextStyle(color: Colors.red),
                      ),
                      const SizedBox(
                        height: 12,
                      ),
                      Text(
                        '${widget.cart.quantity}',
                        style: const TextStyle(
                            fontSize: 12,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      ),
                      const Divider(
                        height: 1,
                        color: Colors.grey,
                      )
                    ],
                  ),
                ],
              ),
            ),
            IconButton(
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => DetailScreen(
                      cart: widget.cart,
                    ),
                  ));
                },
                icon: const Icon(Icons.arrow_forward_ios_outlined, size: 12))
          ],
        ),
      ),
    );
  }
}
