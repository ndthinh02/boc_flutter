import 'package:bloc_test/bloc/cart/cart_bloc.dart';
import 'package:bloc_test/events/cart_event.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../models/foods.dart';

class ItemFoods extends StatefulWidget {
  final Foods foods;
  final int index;
  const ItemFoods({super.key, required this.foods, required this.index});

  @override
  State<ItemFoods> createState() => _ItemFoodsState();
}

class _ItemFoodsState extends State<ItemFoods> {
  // CartController get readCartController => context.read<CartController>();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(2),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Expanded(
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(left: 24, top: 14),
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(8)),
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage(widget.foods.image!))),
                    ),
                    const SizedBox(
                      width: 24,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        const SizedBox(
                          height: 14,
                        ),
                        SizedBox(
                          width: 150,
                          child: Text(
                            widget.foods.id!,
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        const SizedBox(
                          height: 12,
                        ),
                        Text(
                          "\$ ${widget.foods.price!}",
                          style: const TextStyle(color: Colors.red),
                        ),
                        const SizedBox(
                          height: 12,
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Container(
                  margin: const EdgeInsets.only(left: 24, top: 14),
                  child: const Divider(
                    height: 9,
                    color: Colors.grey,
                  ),
                )
              ],
            ),
          ),
          Container(
              margin: const EdgeInsets.only(right: 20, top: 10),
              child: IconButton(
                  onPressed: () {
                    BlocProvider.of<CartBloc>(context).add(AddFoodToCart(
                        name: widget.foods.name!,
                        id: widget.foods.id!,
                        price: widget.foods.price!,
                        description: widget.foods.description!,
                        urlImage: widget.foods.image!,
                        context: context,
                        quantity: 1,
                        index: widget.index));
                  },
                  icon: const Icon(
                    Icons.shopping_cart,
                    color: Colors.blue,
                  )))
        ],
      ),
    );
  }
}
