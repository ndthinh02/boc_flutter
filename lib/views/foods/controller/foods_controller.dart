import 'package:flutter/cupertino.dart';

import '../../../models/foods.dart';

class FoodsController extends ChangeNotifier {
  final List<Foods> _mListFoods = [];
  final TextEditingController nameController = TextEditingController();
  final TextEditingController priceController = TextEditingController();
  final TextEditingController desccriptionController = TextEditingController();
  final TextEditingController urlImageController = TextEditingController();

  addFoods() {
    Foods foods = Foods(
        id: DateTime.now().toString(),
        name: nameController.text,
        image: urlImageController.text,
        price: priceController.text,
        description: desccriptionController.text);
    if (nameController.text.isEmpty ||
        priceController.text.isEmpty ||
        desccriptionController.text.isEmpty ||
        urlImageController.text.isEmpty) {
    } else {
      _mListFoods.add(foods);
      notifyListeners();
      clear();
    }

    notifyListeners();
  }

  clear() {
    nameController.clear();
    priceController.clear();
    desccriptionController.clear();
    urlImageController.clear();
  }

  deleteFoods(int index) {
    _mListFoods.removeAt(index);
    notifyListeners();
  }

  List<Foods> get mListFoods => _mListFoods;
}
