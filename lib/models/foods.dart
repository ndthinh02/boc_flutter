class Foods {
  String? createdAt;
  String? name;
  String? image;
  String? price;
  String? description;
  String? id;

  Foods(
      {this.createdAt,
      this.name,
      this.image,
      this.price,
      this.description,
      this.id});

  Foods.fromJson(Map<String, dynamic> json) {
    createdAt = json['createdAt'];
    name = json['name'];
    image = json['image'];
    price = json['price'];
    description = json['description'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['createdAt'] = createdAt;
    data['name'] = name;
    data['image'] = image;
    data['price'] = price;
    data['description'] = description;
    data['id'] = id;
    return data;
  }
}
