class Cart {
  String name;
  String urlImage;
  String price;
  String description;
  num quantity;
  String id;
  int index;

  Cart(
      {required this.id,
      required this.name,
      required this.urlImage,
      required this.price,
      required this.description,
      required this.quantity,
      required this.index});
}
